//
//  InicioTabBarViewController.swift
//  ExamenIOS
//
//  Created by jonathanmac on 6/13/17.
//  Copyright © 2017 jonathanmac. All rights reserved.
//

import UIKit

class InicioTabBarViewController: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func viewDidAppear(_ animated: Bool) {
        
        let tabBarItems: [UITabBarItem]? = tabBar.items
        
        tabBarItems?[0].title = "Peliculas"
        tabBarItems?[0].image = #imageLiteral(resourceName: "apiexterna")
        
        
        tabBarItems?[1].title = "Guardadas"
        tabBarItems?[1].image = #imageLiteral(resourceName: "apiinterna")
        
    }

}
