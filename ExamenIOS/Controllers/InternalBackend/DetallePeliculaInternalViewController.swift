//
//  DetallePeliculaBackendViewController.swift
//  ExamenIOS
//
//  Created by jonathanmac on 6/14/17.
//  Copyright © 2017 jonathanmac. All rights reserved.
//

import UIKit

class DetallePeliculaInternalViewController: UIViewController {

    
    @IBOutlet weak var tituloPeliculaLabel: UILabel!
    
    @IBOutlet weak var imagenPeliculaImageView: UIImageView!
    
    @IBOutlet weak var anioPeliculaLabel: UILabel!
    
    @IBOutlet weak var categoriaPeliculaLabel: UILabel!
    
    @IBOutlet weak var listaActoresTextView: UITextView!
    
    @IBOutlet weak var resumenPeliculaTextView: UITextView!
    
    var pelicula: Pelicula?
    
    var posterPelicula: UIImage?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tituloPeliculaLabel.text      = pelicula?.titulo!
        imagenPeliculaImageView.image = posterPelicula!
        anioPeliculaLabel.text        = pelicula?.anio!
        categoriaPeliculaLabel.text   = pelicula?.categoria!
        listaActoresTextView.text     = pelicula?.actores!
        resumenPeliculaTextView.text  = pelicula?.resumen!
    }

}
