//
//  PeliculaController.swift
//  ExamenIOS
//
//  Created by CarlosSampedro on 6/14/17.
//  Copyright © 2017 CarlosSampedro. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import AlamofireImage

class PeliculaController {

    private let peliculasExternalBackend: String = "http://netflixroulette.net/api/api.php?director=Quentin%20Tarantino"
    private let peliculasInternalBackend: String = "http://localhost:1337/Pelicula"
    public typealias PeliculasCompletionHandler = (_ : Array<Pelicula>) -> Void
    
    func getAllExternalBackend(completionHandler: @escaping(Array<Pelicula>) -> ()) {
        
        requestPeliculas(url: self.peliculasExternalBackend, completionHandler: completionHandler)

    }
    
    func getAllInternalBackend(completionHandler: @escaping(Array<Pelicula>) -> ()) {
        
        requestPeliculas(url: self.peliculasInternalBackend, completionHandler: completionHandler)
        
    }
    
    func getPoster(pelicula: Pelicula, completionHandler: @escaping(UIImage) -> ()){
        
        Alamofire
            .request(pelicula.poster!)
            .responseImage { response in
                
                guard let imagenPoster = response.result.value else{
                    return
                }

                completionHandler(imagenPoster)
        }
        
    }
    
    func save(pelicula: Pelicula, completionHandler: @escaping(Int) -> ()) {
        
        Alamofire
            .request(self.peliculasInternalBackend, method: .post, parameters: pelicula.toJSON())
            .responseJSON { response in
                
                guard let statusCode = response.response?.statusCode else {
                    print(response.result.error ?? "UNKNOW ERROR")
                    completionHandler(HttpStatusCode.Http503_ServiceUnavailable.rawValue)
                    return
                }
                
                completionHandler(statusCode)
                
        }
        
    }

    func requestPeliculas(url: String, completionHandler: @escaping PeliculasCompletionHandler) {
        
        Alamofire
            .request(url)
            .responseArray { (response: DataResponse<Array<Pelicula>>) in
                
                guard let peliculasResponse = response.result.value else {
                    
                    print("FAIL GET DATA")
                    return
                }
                
                completionHandler(peliculasResponse)
        }
    }
    
}


