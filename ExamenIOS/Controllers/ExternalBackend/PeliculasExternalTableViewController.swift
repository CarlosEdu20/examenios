//
//  PeliculasTableViewController.swift
//  ExamenIOS
//
//  Created by jonathanmac on 6/14/17.
//  Copyright © 2017 jonathanmac. All rights reserved.
//

import UIKit

class PeliculasExternalTableViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var peliculasTableView: UITableView!

    var peliculas: Array<Pelicula> = []
    var posters: [String:UIImage] = [:]
    var peliculaSeleccionada: IndexPath = IndexPath()
    let peliculasController: PeliculaController = PeliculaController()
    let peliculaCellIdentifier: String = "peliculaExternalBackendCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.obtenerPeliculas()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return peliculas.count;
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: peliculaCellIdentifier) as! PeliculaTableViewCell
        let peliculaCelda = peliculas[indexPath.row]
        cell.tituloLabel.text = peliculaCelda.titulo!
        cell.anioLabel.text = peliculaCelda.anio!
        
        obtenerPoster(pelicula: peliculaCelda) { (poster) in

            self.posters[peliculaCelda.titulo!] = poster
            cell.posterImageView.image = poster
            
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
        self.peliculaSeleccionada = indexPath
        return indexPath
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        let destination = segue.destination as! DetallePeliculaExternalViewController
        let pelicula = self.peliculas[peliculaSeleccionada.row]
        destination.pelicula = pelicula
        destination.posterPelicula = self.posters[pelicula.titulo!]
        
    }
    
    func obtenerPeliculas() {
        
        peliculasController.getAllExternalBackend { (peliculasObtenidas) in
            
            self.peliculas = peliculasObtenidas
            self.refrescarTablaPeliculas()

        }
        
    }
    
    func obtenerPoster(pelicula: Pelicula, completionHandler: @escaping(UIImage)->()){
        
        self.peliculasController.getPoster(pelicula: pelicula) { (posterObtenido) in
            completionHandler(posterObtenido)
        }
    }
    
    
    func refrescarTablaPeliculas() {
        
        DispatchQueue.main.async { 
            
            self.peliculasTableView.reloadData()

        }
    }

}
