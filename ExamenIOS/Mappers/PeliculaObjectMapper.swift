//
//  PeliculaObjectMapper.swift
//  ExamenIOS
//
//  Created by CarlosSampedro on 6/14/17.
//  Copyright © 2017 CarlosSampedro. All rights reserved.
//

import Foundation
import ObjectMapper


class Pelicula: Mappable {
    
    var titulo: String?
    var anio: String?
    var categoria: String?
    var actores: String?
    var resumen: String?
    var poster: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        
        titulo    <- map["show_title"]
        anio      <- map["release_year"]
        categoria <- map["category"]
        actores   <- map["show_cast"]
        resumen   <- map["summary"]
        poster    <- map["poster"]
    }
}
