//
//  DetallePeliculaViewController.swift
//  ExamenIOS
//
//  Created by jonathanmac on 6/14/17.
//  Copyright © 2017 jonathanmac. All rights reserved.
//

import UIKit
import Toast_Swift

class DetallePeliculaExternalViewController: UIViewController {

    @IBOutlet weak var tituloPeliculaLabel: UILabel!

    @IBOutlet weak var imagenPeliculaImageView: UIImageView!
    
    @IBOutlet weak var anioPeliculaLabel: UILabel!
    
    @IBOutlet weak var categoriaPeliculaLabel: UILabel!
    
    @IBOutlet weak var listaActoresTextView: UITextView!
    
    @IBOutlet weak var resumenPeliculaTextView: UITextView!
    
    var pelicula: Pelicula?
    
    var posterPelicula: UIImage?
    
    let peliculaController: PeliculaController = PeliculaController()
    
    
    @IBAction func guardarPeliculaButton(_ sender: Any) {
        
        peliculaController.save(pelicula: self.pelicula!) { (statusCode) in
            
            if (statusCode == HttpStatusCode.Http201_Created.rawValue) {
                
                self.view.makeToast("Pelicula Guardada", duration: 3.0, position: .center)
            }
            else if (statusCode == HttpStatusCode.Http409_Conflict.rawValue){
                
                self.view.makeToast("Ya existe la pelicula", duration: 3.0, position: .center)
            }
            else {
                self.view.makeToast("Error al guardar la pelicula", duration: 3.0, position: .center)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        tituloPeliculaLabel.text      = pelicula?.titulo!
        imagenPeliculaImageView.image = posterPelicula!
        anioPeliculaLabel.text        = pelicula?.anio!
        categoriaPeliculaLabel.text   = pelicula?.categoria!
        listaActoresTextView.text     = pelicula?.actores!
        resumenPeliculaTextView.text  = pelicula?.resumen!
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    


}
