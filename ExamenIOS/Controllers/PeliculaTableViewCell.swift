//
//  PeliculaTableViewCell.swift
//  ExamenIOS
//
//  Created by jonathanmac on 6/14/17.
//  Copyright © 2017 jonathanmac. All rights reserved.
//

import UIKit

class PeliculaTableViewCell: UITableViewCell {


    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var anioLabel: UILabel!
    @IBOutlet weak var posterImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
